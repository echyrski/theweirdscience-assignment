**Battle Ships**

This is browser game for two players.

Each player has 10 ships:
1. One 4-cell ship
2. Two 3-cell ships
3. Three 2-cell ships
4. Four 1-cell ships

Each player places his ships on the field 10x10 cells.

Turn by turn player tries to bomb field of his opponent in order to blow all his ships.
The first player who finds all ships placed by his opponent wins.

Server starts on port 8089


Assignment:


