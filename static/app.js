var USERID = undefined;
var converted = 'oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo';

function setCharAt(str, index, chr) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
}
$(function () {
    var claimNoUserSuccess = function (resp) {
        if (resp) {
            location.reload();
        } else {
            alert("Please give you opponent a minute or two.");
        }
    };
    var subscribeBroadcast = function (gameId) {
        var request = {
            url: "/game/" + gameId,
            maxReconnectOnClose: 100000000,
            reconnectInterval: 10000,
            trackMessageLength: false,
            transport: 'streaming'
        };
        request.onError = function (response) {
            console.log(response);
        };

        request.onDisconnect = function (response) {
            console.log(response);

        };


        request.onMessage = function (response) {
            console.log(response);
            detectedTransport = response.transport;
            if (response.status == 200) {
                var data = response.responseBody;
                if (data.length > 0) {
                    try {
                        var json = JSON.parse(data);
                        if (json.length != undefined) {
                            json.forEach(function (e) {
                                processNotificationJson(e)
                            })
                        } else {
                            processNotificationJson(json)
                        }


                    } catch (err) {
                        console.log(err)
                    }

                }
            }
        };
        var subSocket = atmosphere.subscribe(request);
    };
    var processNotificationJson = function (json) {
        if (json.type === "start") {
            if (USERID === json.userIdStarts) {
                $("#battle-field-2").attr('disabled', '');
                alert("Please start!");
            } else {
                $("#battle-field-2").attr('disabled', 'disabled');

            }
        } else if (json.type === "hit") {

            if (json.userDefendant === USERID) {


                $("div[cellX1=" + json.x + "][cellY1=" + json.y + "]").attr('class', '');
                $("div[cellX1=" + json.x + "][cellY1=" + json.y + "]").addClass(json.success ? "d" : "m");
                if (json.won) {
                    alert("Looser :(");
                    location.reload();
                }
            } else {
                $("div[cellX2=" + json.x + "][cellY2=" + json.y + "]").attr('class', '');
                $("div[cellX2=" + json.x + "][cellY2=" + json.y + "]").addClass(json.success ? "d" : "m");
                if (json.won) {
                    alert("Congrats,bro!");
                    location.reload();

                }
            }

        }
    };
    var onGameStartSuccess = function (resp) {
        $("#claimNoUser").show();

        var gameId = resp.gameid;
        var imediateStart = resp.immediateStart;
        var startsThisUser = resp.userIdStarts === USERID;

        p1 = document.getElementById('battle-field-1');
        p2 = document.getElementById('battle-field-2');
        $(".draggable").hide();
        $("#validateAndStart").hide();
        for (i = 0; i < 10; i++) for (j = 0; j < 10; j++) {
            div1 = document.createElement('div');
            div1.id = i + '_' + j;
            div1.setAttribute("cellX1", i);
            div1.setAttribute("cellY1", j);
            if (converted[i * 10 + j] == 'x') {
                div1.className = 's';
            } else {
                div1.className = 'w';
            }
            p1.appendChild(div1);
            div2 = document.createElement('div');
            div2.className = 'w';
            div2.setAttribute("cellX2", i);
            div2.setAttribute("cellY2", j);
            div2.onclick = function () {
                fire(this);
            };
            p2.appendChild(div2);
        }


        subscribeBroadcast(gameId);


        $("#battle-field-2").attr('disabled', 'disabled');
        if (imediateStart) {

            if (startsThisUser) {
                $("#battle-field-2").attr('disabled', '');
            }
        } else {
            alert("Please wait for opponent!");
        }

    };
    var fire = function (el) {
        var x = el.getAttribute("cellX2");
        var y = el.getAttribute("cellY2");
        $.get("/api/hit?x=" + x + "&y=" + y);
    };
    var onGameStartError = function (resp) {
        alert(JSON.parse(resp.responseText).message);
    };
    var onUserIdSuccess = function (resp) {
        USERID = resp.id;
        $.post({
            url: "/api/start",
            data: converted,
            dataType: "json",
            success: onGameStartSuccess,
            error: onGameStartError
        })
    };
    var validateAndStart = function () {

        var elems = [];
        $(".draggable").each(function (id, obj) {
            elems.push(obj.getBoundingClientRect());
        });
        for (i = 0; i < elems.length; i++) {
            var startHorizontalCell = Math.min(elems[i].left / 32, elems[i].right / 32);
            var endHorizontalCell = Math.max(elems[i].left / 32, elems[i].right / 32);

            var startVerticalCell = Math.min(elems[i].top / 32, elems[i].bottom / 32);
            var endVerticalCell = Math.max(elems[i].top / 32, elems[i].bottom / 32);

            var alignedHorizontally = endVerticalCell - startVerticalCell === 1;
            if (alignedHorizontally) {
                var verticalPosition = Math.ceil(startVerticalCell) - 1;
                for (j = Math.ceil(startHorizontalCell) - 1; j < Math.ceil(endHorizontalCell) - 1; j++) {
                    converted = setCharAt(converted, verticalPosition * 10 + j, 'x');
                }
            } else {
                var horizontalPosition = Math.ceil(startHorizontalCell) - 1;
                for (j = Math.ceil(startVerticalCell) - 1; j < Math.ceil(endVerticalCell) - 1; j++) {
                    converted = setCharAt(converted, j * 10 + horizontalPosition, 'x');
                }
            }
            for (j = 0; j < elems.length; j++) {
                if (i != j) {

                    var overlap = !(elems[i].right < elems[j].left ||
                    elems[i].left > elems[j].right ||
                    elems[i].bottom < elems[j].top || elems[i].top > elems[j].bottom);
                    if (overlap) {
                        alert("Please fix ship overlap!");
                        return;
                    }
                }
            }
        }
        ;

        $.get("/api/id", onUserIdSuccess);

    }

    $("#claimNoUser").hide();
    $("#claimNoUser").click(function () {
        $.get("/api/claim-win", claimNoUserSuccess);
    });
    //rotation
    $(".draggable").dblclick(function () {
        var width = $(this).width();
        $(this).width($(this).height());
        $(this).height(width);
    });


    $(".draggable").draggable({containment: "parent", grid: [32, 32]});

    $("#validateAndStart").click(validateAndStart)

});


