package me.echyrski.assignments.theweirdscience.atmosphere;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.atmosphere.config.service.AtmosphereHandlerService;
import org.atmosphere.config.service.Singleton;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.handler.AtmosphereHandlerAdapter;

import java.io.IOException;

@Singleton
public class PubSubBroadcaster extends AtmosphereHandlerAdapter {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void onStateChange(AtmosphereResourceEvent event) throws IOException {
        if (event.isSuspended()) {
            if (event.getMessage() != null) {
                event.getResource().write(mapper.writeValueAsString(event.getMessage()));
            }
        }
    }
}
