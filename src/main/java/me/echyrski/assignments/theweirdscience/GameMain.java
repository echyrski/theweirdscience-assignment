package me.echyrski.assignments.theweirdscience;

import me.echyrski.assignments.theweirdscience.cfg.WebConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * @author echyrski
 */
@SpringBootApplication
@ComponentScan("me.echyrski.assignments.theweirdscience.api")
@Import({WebConfig.class})
@PropertySource({
        "classpath:application.properties"
})
public class GameMain {
    public static void main(String[] args) {
       SpringApplication.run(GameMain.class, args);
    }
}
