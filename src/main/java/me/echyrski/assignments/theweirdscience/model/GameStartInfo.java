package me.echyrski.assignments.theweirdscience.model;

import com.google.common.base.MoreObjects;

/**
 * @author echyrski
 */
public class GameStartInfo {

    private String gameid;
    private boolean immediateStart;
    private String userIdStarts;
    private String type = "start";

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid;
    }

    public boolean isImmediateStart() {
        return immediateStart;
    }

    public void setImmediateStart(boolean immediateStart) {
        this.immediateStart = immediateStart;
    }

    public String getUserIdStarts() {
        return userIdStarts;
    }

    public void setUserIdStarts(String userIdStarts) {
        this.userIdStarts = userIdStarts;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("gameid", gameid)
                .add("immediateStart", immediateStart)
                .add("userIdStarts", userIdStarts)
                .add("type", type)
                .toString();
    }
}
