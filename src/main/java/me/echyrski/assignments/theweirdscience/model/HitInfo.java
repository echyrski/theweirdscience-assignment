package me.echyrski.assignments.theweirdscience.model;

import com.google.common.base.MoreObjects;

/**
 * Information about hit made by user
 *
 * @author echyrski
 */
public class HitInfo {
    private int x;
    private int y;
    private boolean success;
    private String userDefendant;
    private boolean won;
    private String type = "hit";


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getUserDefendant() {
        return userDefendant;
    }

    public void setUserDefendant(String userDefendant) {
        this.userDefendant = userDefendant;
    }

    public boolean isWon() {
        return won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("x", x)
                .add("y", y)
                .add("success", success)
                .add("userDefendant", userDefendant)
                .add("won", won)
                .add("type", type)
                .toString();
    }
}
