package me.echyrski.assignments.theweirdscience.model;

import com.google.common.base.MoreObjects;

import java.util.Arrays;

/**
 * @author echyrski
 */
public class ShipGame {
    private Boolean[][] userOneBoard;
    private Boolean[][] userTwoBoard;
    private String userOneId;
    private String userTwoId;
    private String gameId;
    private long lastUserOneSeen;
    private long lastUserTwoSeen;
    private boolean firstUserTurn = true;
    private byte userOneSuccessCount = 0;
    private byte userTwoSuccessCount = 0;

    public Boolean[][] getUserOneBoard() {
        return userOneBoard;
    }

    public void setUserOneBoard(Boolean[][] userOneBoard) {
        this.userOneBoard = userOneBoard;
    }

    public Boolean[][] getUserTwoBoard() {
        return userTwoBoard;
    }

    public void setUserTwoBoard(Boolean[][] userTwoBoard) {
        this.userTwoBoard = userTwoBoard;
    }

    public String getUserOneId() {
        return userOneId;
    }

    public void setUserOneId(String userOneId) {
        this.userOneId = userOneId;
    }

    public String getUserTwoId() {
        return userTwoId;
    }

    public void setUserTwoId(String userTwoId) {
        this.userTwoId = userTwoId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public long getLastUserOneSeen() {
        return lastUserOneSeen;
    }

    public void setLastUserOneSeen(long lastUserOneSeen) {
        this.lastUserOneSeen = lastUserOneSeen;
    }

    public long getLastUserTwoSeen() {
        return lastUserTwoSeen;
    }

    public void setLastUserTwoSeen(long lastUserTwoSeen) {
        this.lastUserTwoSeen = lastUserTwoSeen;
    }

    public boolean isFirstUserTurn() {
        return firstUserTurn;
    }

    public void setFirstUserTurn(boolean firstUserTurn) {
        this.firstUserTurn = firstUserTurn;
    }

    public byte getUserOneSuccessCount() {
        return userOneSuccessCount;
    }

    public void setUserOneSuccessCount(byte userOneSuccessCount) {
        this.userOneSuccessCount = userOneSuccessCount;
    }

    public byte getUserTwoSuccessCount() {
        return userTwoSuccessCount;
    }

    public void setUserTwoSuccessCount(byte userTwoSuccessCount) {
        this.userTwoSuccessCount = userTwoSuccessCount;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userOneBoard", userOneBoard)
                .add("userTwoBoard", userTwoBoard)
                .add("userOneId", userOneId)
                .add("userTwoId", userTwoId)
                .add("gameId", gameId)
                .add("lastUserOneSeen", lastUserOneSeen)
                .add("lastUserTwoSeen", lastUserTwoSeen)
                .add("firstUserTurn", firstUserTurn)
                .add("userOneSuccessCount", userOneSuccessCount)
                .add("userTwoSuccessCount", userTwoSuccessCount)
                .toString();
    }
}
