package me.echyrski.assignments.theweirdscience.model;

import com.google.common.base.MoreObjects;

/**
 * @author echyrski
 */
public class UserId {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .toString();
    }
}
