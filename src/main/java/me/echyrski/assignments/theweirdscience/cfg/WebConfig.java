package me.echyrski.assignments.theweirdscience.cfg;

import me.echyrski.assignments.theweirdscience.atmosphere.PubSubBroadcaster;
import me.echyrski.assignments.theweirdscience.service.GameCommunicationService;
import me.echyrski.assignments.theweirdscience.service.GameStorageService;
import me.echyrski.assignments.theweirdscience.service.impl.AtmosphereGameCommunicationServiceImpl;
import me.echyrski.assignments.theweirdscience.service.impl.InMemoryGameStorageService;
import org.atmosphere.cache.UUIDBroadcasterCache;
import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.cpr.AnnotationProcessor;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereInterceptor;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.atmosphere.interceptor.BroadcastOnPostAtmosphereInterceptor;
import org.atmosphere.interceptor.SuspendTrackerInterceptor;
import org.atmosphere.spring.bean.AtmosphereSpringContext;
import org.atmosphere.spring.bean.AtmosphereSpringServlet;
import org.atmosphere.util.VoidAnnotationProcessor;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.atmosphere.cpr.BroadcasterLifeCyclePolicy.ATMOSPHERE_RESOURCE_POLICY.IDLE_DESTROY;

/**
 * @author echyrski
 */

@ComponentScan("me.echyrski.assignments.theweirdscience.api")
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public AtmosphereFramework atmosphereFramework(ServletRegistrationBean atmosphereServlet)
            throws ServletException, InstantiationException, IllegalAccessException {
        AtmosphereFramework atmosphereFramework = new AtmosphereFramework(false, false);
        atmosphereFramework.setBroadcasterCacheClassName(UUIDBroadcasterCache.class.getName());
        return atmosphereFramework;
    }

    @Bean
    public PubSubBroadcaster hazelcastHandler() {
        return new PubSubBroadcaster();
    }

    @Bean
    public BroadcasterFactory broadcasterFactory(AtmosphereFramework atmosphereFramework)
            throws ServletException, InstantiationException, IllegalAccessException {
        return atmosphereFramework.getAtmosphereConfig().getBroadcasterFactory();
    }

    @Bean
    public AtmosphereSpringContext atmosphereSpringContext() {
        AtmosphereSpringContext atmosphereSpringContext = new AtmosphereSpringContext();
        Map<String, String> map = new HashMap<>();
        map.put("org.atmosphere.cpr.broadcasterClass", org.atmosphere.cpr.DefaultBroadcaster.class.getName());
        map.put(AtmosphereInterceptor.class.getName(), TrackMessageSizeInterceptor.class.getName());
        map.put(AnnotationProcessor.class.getName(), VoidAnnotationProcessor.class.getName());
        map.put("org.atmosphere.cpr.broadcasterLifeCyclePolicy", IDLE_DESTROY.toString());
        atmosphereSpringContext.setConfig(map);
        return atmosphereSpringContext;
    }

    @Bean
    private AtmosphereSpringServlet atmosphereSpringServlet(){
        return new AtmosphereSpringServlet();
    }

    @Bean
    public ServletRegistrationBean atmosphereServlet() {

        ServletRegistrationBean registration = new ServletRegistrationBean(
                atmosphereSpringServlet(), "/game/*");

        return registration;
    }

    @Bean
    GameStorageService gameStorageService() {
        return new InMemoryGameStorageService();
    }

    @Bean
    public GameCommunicationService gameCommunicationService(AtmosphereFramework framework) {
        return new AtmosphereGameCommunicationServiceImpl(framework, gameStorageService());
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/index.html");
    }
}
