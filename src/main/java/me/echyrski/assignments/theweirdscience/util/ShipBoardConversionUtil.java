package me.echyrski.assignments.theweirdscience.util;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.springframework.util.Assert;

import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;
import static me.echyrski.assignments.theweirdscience.util.ApplicationConstants.BOARD_AST_SIZE;
import static me.echyrski.assignments.theweirdscience.util.ApplicationConstants.BOARD_CELL_COUNT;

/**
 * @author echyrski
 */
public final class ShipBoardConversionUtil {


    private ShipBoardConversionUtil() {
        throw new RuntimeException("Not to be instantiated");
    }

    /**
     * Converts String representation of the board into Array representation
     *
     * @param board
     * @return
     */
    public static Boolean[][] convertBoard(String board) {
        Assert.notNull(board, "Board must not be defined correctly!");
        Assert.isTrue(board.length() == BOARD_CELL_COUNT, "Board size must be " + BOARD_CELL_COUNT + " cells");

        return Arrays.stream(
                Iterables.toArray(
                        Splitter.fixedLength(BOARD_AST_SIZE)
                                .split(board)
                        , String.class))
                .map(ShipBoardConversionUtil::convertBoardLine)
                .toArray(Boolean[][]::new);

    }

    /**
     * Converts array representation of the board into string representation
     *
     * @param board
     * @return
     */
    public static String convertBoard(Boolean[][] board) {
        Assert.notNull(board, "Board must not be defined correctly!");

        String line = Arrays.stream(board).flatMap(l -> Arrays.stream(l)).map(e -> e ? 'x' : 'o').collect(StringBuilder::new,
                StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        Assert.isTrue(line.length() == BOARD_CELL_COUNT, "Invalid board size. Must be " + BOARD_CELL_COUNT);
        return line;


    }

    private static Boolean[] convertBoardLine(String line) {
        return CharBuffer.wrap(line.toCharArray()).chars().mapToObj(ch -> 'x' == ch).toArray(Boolean[]::new);
    }
}
