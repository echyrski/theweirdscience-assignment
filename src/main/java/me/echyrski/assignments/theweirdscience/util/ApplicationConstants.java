package me.echyrski.assignments.theweirdscience.util;

import java.util.concurrent.TimeUnit;

/**
 * @author echyrski
 */
public final class ApplicationConstants {

    private ApplicationConstants() {
        throw new RuntimeException("Not to be instantiated");
    }

    public static int MAX_HIT_COUNT = 20;
    public static final int BOARD_AST_SIZE = 10;
    public static final long MAX_PLAYER_AWAIT_TIME = TimeUnit.MINUTES.toMillis(1);
    public static final int BOARD_CELL_COUNT = BOARD_AST_SIZE * BOARD_AST_SIZE;
}
