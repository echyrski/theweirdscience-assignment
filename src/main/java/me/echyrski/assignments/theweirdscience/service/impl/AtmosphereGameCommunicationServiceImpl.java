package me.echyrski.assignments.theweirdscience.service.impl;

import me.echyrski.assignments.theweirdscience.atmosphere.PubSubBroadcaster;
import me.echyrski.assignments.theweirdscience.model.GameStartInfo;
import me.echyrski.assignments.theweirdscience.model.HitInfo;
import me.echyrski.assignments.theweirdscience.model.ShipGame;
import me.echyrski.assignments.theweirdscience.service.GameCommunicationService;
import me.echyrski.assignments.theweirdscience.service.GameStorageService;
import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereInterceptor;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.atmosphere.interceptor.BroadcastOnPostAtmosphereInterceptor;
import org.atmosphere.interceptor.SuspendTrackerInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static me.echyrski.assignments.theweirdscience.util.ApplicationConstants.MAX_HIT_COUNT;

/**
 * Implementation of {@see GameCommunicationService} based on Atmospehere framework for user communication
 *
 * @author echyrski
 */
public class AtmosphereGameCommunicationServiceImpl implements GameCommunicationService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AtmosphereFramework framework;
    private final GameStorageService gameStorageService;

    public AtmosphereGameCommunicationServiceImpl(AtmosphereFramework framework, GameStorageService gameStorageService) {
        Assert.notNull(framework, "AtmosphereFramework must not be null");
        Assert.notNull(gameStorageService, "GameStorageService must not be null");
        this.framework = framework;
        this.gameStorageService = gameStorageService;
    }

    public GameStartInfo createNewGame(String userId, Boolean[][] boardConfig) {
        GameStartInfo res = new GameStartInfo();
        ShipGame shipGame = gameStorageService.getGame(userId);
        if (shipGame != null) {

            Assert.isNull(shipGame.getUserTwoId());
            Assert.isNull(shipGame.getUserTwoBoard());
            shipGame.setUserTwoId(userId);
            shipGame.setUserTwoBoard(boardConfig);
            shipGame.setLastUserTwoSeen(System.currentTimeMillis());
            res.setGameid(shipGame.getGameId());
            res.setImmediateStart(true);
            res.setUserIdStarts(shipGame.getUserOneId());
            broadcast(shipGame.getGameId(), res); //broadcasting first user only

        } else {
            String gameId = UUID.randomUUID().toString();
            shipGame = new ShipGame();
            shipGame.setGameId(gameId);
            shipGame.setLastUserOneSeen(System.currentTimeMillis());
            shipGame.setUserOneBoard(boardConfig);
            shipGame.setUserOneId(userId);
            gameStorageService.addGame(shipGame);
            framework.addAtmosphereHandler("/game/" + gameId, new PubSubBroadcaster(), interceptors());
            res.setGameid(shipGame.getGameId());
            res.setImmediateStart(false);
        }

        return res;
    }

    @Override
    public void hit(String userId, int x, int y) {
        ShipGame game = gameStorageService.getGameByUserId(userId);
        if (game == null) {
            throw new RuntimeException("Game Complete!");
        }
        String userIdTurn = game.isFirstUserTurn() ? game.getUserOneId() : game.getUserTwoId();
        if (!userId.equals(userIdTurn)) {
            throw new RuntimeException("Not your turn");
        }

        Boolean[][] board = game.isFirstUserTurn() ? game.getUserTwoBoard() : game.getUserOneBoard();
        boolean hit = board[x][y];
        HitInfo hitInfo = new HitInfo();
        hitInfo.setX(x);
        hitInfo.setY(y);
        hitInfo.setSuccess(hit);


        if (game.isFirstUserTurn()) {
            game.setLastUserOneSeen(System.currentTimeMillis());
            hitInfo.setUserDefendant(game.isFirstUserTurn() ? game.getUserTwoId() : game.getUserOneId());
            if (hit) {
                game.setUserOneSuccessCount((byte) (game.getUserOneSuccessCount() + 1));
            }
            hitInfo.setWon(game.getUserOneSuccessCount() == MAX_HIT_COUNT);

        } else {
            game.setLastUserTwoSeen(System.currentTimeMillis());
            hitInfo.setUserDefendant(game.isFirstUserTurn() ? game.getUserTwoId() : game.getUserOneId());
            if (hit) {
                game.setUserTwoSuccessCount((byte) (game.getUserTwoSuccessCount() + 1));
            }
            hitInfo.setWon(game.getUserTwoSuccessCount() == MAX_HIT_COUNT);

        }

        if (hitInfo.isWon()) {
            gameStorageService.cleanGame(userId);
        }


        game.setFirstUserTurn(!game.isFirstUserTurn());
        broadcast(game.getGameId(), hitInfo);
    }

    @Override
    public boolean absenceWin(String userId) {
        boolean resp = gameStorageService.checkOpponentTimeout(userId);
        if (resp) {
            gameStorageService.cleanGame(userId);
        }
        return resp;
    }


    private void broadcast(String gameid, Object message) {
        Broadcaster broadcaster = framework.getBroadcasterFactory().lookup("/game/" + gameid);
        if (broadcaster != null) {
            logger.debug("broadcasting {}", message);
            broadcaster.broadcast(message);
        } else {
            logger.error("Unable to broadcast message {} for game :{}", message, gameid);
        }
    }


    private static List<AtmosphereInterceptor> interceptors() {
        List<AtmosphereInterceptor> atmosphereInterceptors = new ArrayList<>();
        atmosphereInterceptors.add(new AtmosphereResourceLifecycleInterceptor());
        atmosphereInterceptors.add(new TrackMessageSizeInterceptor());
        atmosphereInterceptors.add(new BroadcastOnPostAtmosphereInterceptor());
        atmosphereInterceptors.add(new SuspendTrackerInterceptor());
        return atmosphereInterceptors;
    }
}
