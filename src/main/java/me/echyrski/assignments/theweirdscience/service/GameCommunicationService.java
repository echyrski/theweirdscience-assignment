package me.echyrski.assignments.theweirdscience.service;

import me.echyrski.assignments.theweirdscience.model.GameStartInfo;
import me.echyrski.assignments.theweirdscience.model.HitInfo;

/**
 * @author echyrski
 */
public interface GameCommunicationService {
    /**
     * If no gemes exists in the waiting list creates new game and posts it into the waiting list.
     * Otherwise starts new game immediately based on the data in the waiting list
     *
     * @param userId
     * @param boardConfig
     * @return
     */
    GameStartInfo createNewGame(String userId, Boolean[][] boardConfig);

    /**
     * Hit cell request
     *
     * @param userId
     * @param x
     * @param y
     */
    void hit(String userId, int x, int y);

    /**
     * Returns true if second player left the game
     *
     * @param userId
     * @return
     */
    boolean absenceWin(String userId);

}
