package me.echyrski.assignments.theweirdscience.service.impl;

import com.google.common.collect.Interner;
import com.google.common.collect.Interners;
import me.echyrski.assignments.theweirdscience.model.ShipGame;
import me.echyrski.assignments.theweirdscience.service.GameStorageService;
import me.echyrski.assignments.theweirdscience.util.ApplicationConstants;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Implementation of the {@link GameStorageService} which stores game data in memory
 *
 * @author echyrski
 */
public class InMemoryGameStorageService implements GameStorageService {
    private Interner<String> interner = Interners.newStrongInterner();
    private final ConcurrentLinkedDeque<ShipGame> awaitingGames = new ConcurrentLinkedDeque<>();
    private final Map<String, ShipGame> currentUsersInGame = new ConcurrentHashMap<>();


    @Override
    public ShipGame getGame(String userId) {
        Assert.notNull(userId, "User id must be set");
        String internedUserId = getInternedUserId(userId);
        synchronized (internedUserId) {
            if (currentUsersInGame.containsKey(internedUserId)) {
                throw new RuntimeException("You are already playing. Please finish current game");
            }
            ShipGame game = awaitingGames.poll();
            if (game != null) {
                currentUsersInGame.put(internedUserId, game);
                return game;
            }
        }
        return null;
    }

    @Override
    public boolean addGame(ShipGame game) {
        Assert.notNull(game, "Game info must not be null");
        Assert.notNull(game.getUserOneId(), "Game creator must be defined");
        String userId = getInternedUserId(game.getUserOneId());
        synchronized (userId) {
            if (currentUsersInGame.containsKey(userId)) {
                return false;
            }
            boolean offered = awaitingGames.offer(game);
            if (offered) {
                currentUsersInGame.put(userId, game);
            }
            return offered;
        }

    }


    @Override
    public ShipGame getGameByUserId(String userid) {
        return currentUsersInGame.get(getInternedUserId(userid));
    }

    @Override
    public void cleanGame(String userId) {
        ShipGame game = getGameByUserId(getInternedUserId(userId));
        currentUsersInGame.remove(getInternedUserId(game.getUserOneId()));
        if (game.getUserTwoId() != null) {
            currentUsersInGame.remove(getInternedUserId(game.getUserTwoId()));
        }
    }

    @Override
    public boolean checkOpponentTimeout(String userId) {
        Assert.notNull(userId, "User id must not be null");
        ShipGame game = getGameByUserId(getInternedUserId(userId));
        if (game == null) throw new RuntimeException("Game complete!");
        boolean checkSecondUser = userId.equals(game.getUserOneId());
        long lastSeenOpponentTime = checkSecondUser ? game.getLastUserTwoSeen() : game.getLastUserOneSeen();
        return System.currentTimeMillis() - lastSeenOpponentTime > ApplicationConstants.MAX_PLAYER_AWAIT_TIME;
    }

    private String getInternedUserId(String userId) {
        return interner.intern(new String(userId));
    }
}
