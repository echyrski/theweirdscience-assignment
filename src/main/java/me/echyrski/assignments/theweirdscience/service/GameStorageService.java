package me.echyrski.assignments.theweirdscience.service;

import me.echyrski.assignments.theweirdscience.model.ShipGame;

/**
 * @author echyrski
 */
public interface GameStorageService {

    /**
     * Get game from the waiting list
     *
     * @return
     */
    ShipGame getGame(String userid);

    /**
     * Add game request into queue for second player search
     *
     * @param game
     * @return
     */
    boolean addGame(ShipGame game);

    /**
     * Return game user currently playing
     *
     * @param userid
     * @return
     */
    ShipGame getGameByUserId(String userid);

    /**
     * Remove game from storage
     *
     * @param userId
     */
    void cleanGame(String userId);

    /**
     * Check if secod player made a move within last minute
     *
     * @param userId
     * @return
     */
    boolean checkOpponentTimeout(String userId);


}
