package me.echyrski.assignments.theweirdscience.api;

import me.echyrski.assignments.theweirdscience.model.GameStartInfo;
import me.echyrski.assignments.theweirdscience.model.UserId;
import me.echyrski.assignments.theweirdscience.service.GameCommunicationService;
import me.echyrski.assignments.theweirdscience.util.ShipBoardConversionUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author echyrski
 */

@RestController
@RequestMapping("/api")
public class CommunicationApi {

    @Autowired
    private GameCommunicationService gameCommunicationService;

    @RequestMapping(value = "/start", produces = "application/json", method = POST)
    @ResponseBody
    public GameStartInfo startGame(@RequestBody String config, HttpSession session) {
        String userId = session.getId();
        return gameCommunicationService.createNewGame(userId, ShipBoardConversionUtil.convertBoard(config.replace("=", "")));

    }

    @RequestMapping(value = "/id", produces = "application/json", method = GET)
    @ResponseBody
    public UserId getMyid(HttpSession session) {
        //TODO implement login procedure and replace with real user id;
        UserId id = new UserId();
        id.setId(session.getId());
        return id;

    }


    @RequestMapping(value = "/claim-win", produces = "application/json", method = GET)
    @ResponseBody
    public boolean claimAbsenceWin(HttpSession session) {
        return gameCommunicationService.absenceWin(session.getId());

    }

    @RequestMapping(value = "/hit", produces = "application/json", method = GET)
    @ResponseBody
    public void hitOpponent(@RequestParam("x") int x, @RequestParam("y") int y, HttpSession session) {
        Assert.isTrue(x >= 0, "Invalid hit position");
        Assert.isTrue(y >= 0, "Invalid hit position");
        Assert.isTrue(x < 10, "Invalid hit position");
        Assert.isTrue(y < 10, "Invalid hit position");
        String userId = session.getId();
        gameCommunicationService.hit(userId, x, y);
    }


}
