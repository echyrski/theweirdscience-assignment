package me.echyrski.assignments.theweirdscience.util;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.junit.Assert;
import org.junit.Test;

import static me.echyrski.assignments.theweirdscience.util.ApplicationConstants.BOARD_AST_SIZE;

/**
 * @author echyrski
 */
public class ShipBoardConversionUtilTest {
    Boolean[][] data = new Boolean[][]{
            new Boolean[]{false, false, false, false, false, false, false, false, false, false},
            new Boolean[]{false, false, false, false, false, false, false, false, false, false},
            new Boolean[]{false, false, true, false, false, false, false, false, false, false},
            new Boolean[]{false, false, true, false, false, false, false, false, false, false},
            new Boolean[]{false, false, true, false, false, false, false, false, false, false},
            new Boolean[]{false, false, true, false, false, true, false, true, false, false},
            new Boolean[]{false, false, false, false, false, false, false, false, false, false},
            new Boolean[]{false, false, false, false, false, false, false, false, false, false},
            new Boolean[]{false, false, false, false, false, false, false, false, false, false},
            new Boolean[]{false, false, false, false, false, false, false, false, false, false},
    };

    @Test
    public void testConversion() {
        String board = ShipBoardConversionUtil.convertBoard(data);
        String boardSplit[] = Iterables.toArray(
                Splitter.fixedLength(BOARD_AST_SIZE)
                        .split(board)
                , String.class);
        Assert.assertEquals(boardSplit[0], "oooooooooo");
        Assert.assertEquals(boardSplit[1], "oooooooooo");
        Assert.assertEquals(boardSplit[2], "ooxooooooo");
        Assert.assertEquals(boardSplit[3], "ooxooooooo");
        Assert.assertEquals(boardSplit[4], "ooxooooooo");
        Assert.assertEquals(boardSplit[5], "ooxooxoxoo");
        Assert.assertEquals(boardSplit[6], "oooooooooo");
        Assert.assertEquals(boardSplit[7], "oooooooooo");
        Assert.assertEquals(boardSplit[8], "oooooooooo");
        Assert.assertEquals(boardSplit[9], "oooooooooo");

        Boolean[][] converted = ShipBoardConversionUtil.convertBoard(board);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Assert.assertTrue(data[i][j].equals(converted[i][j]));
            }

        }
    }
}
