package me.echyrski.assignments.theweirdscience.service.impl;

import me.echyrski.assignments.theweirdscience.model.ShipGame;
import org.junit.Assert;
import org.junit.Test;

import javax.validation.constraints.AssertTrue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

/**
 * @author echyrski
 */
public class InMemoryGameStorageServiceTest {

    @Test
    public void testAddGame() throws Exception {

        InMemoryGameStorageService service = new InMemoryGameStorageService();
        List<CompletableFuture<Boolean>> list = new ArrayList<>(100);
        for (int i = 0; i < 100; i++) {
            list.add(CompletableFuture.supplyAsync(() -> {
                ShipGame game = new ShipGame();
                game.setUserOneId("userOne");
                return service.addGame(game);
            }));
        }

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(list.toArray(new CompletableFuture[0]));
        allFutures.get();
        Map<Boolean, AtomicInteger> res = new HashMap<>();
        res.put(true, new AtomicInteger(0));
        res.put(false, new AtomicInteger(0));
        for (CompletableFuture<Boolean> future : list) {
            res.get(future.get()).incrementAndGet();
        }
        Assert.assertEquals(res.get(Boolean.TRUE).get(), 1);
        Assert.assertEquals(res.get(Boolean.FALSE).get(), 99);

    }

    @Test
    public void testGetGameSuccess() throws Exception {
        InMemoryGameStorageService service = new InMemoryGameStorageService();
        List<CompletableFuture<Boolean>> list = new ArrayList<>(100);
        for (int i = 0; i < 100; i++) {
            final int userId = i;
            list.add(CompletableFuture.supplyAsync(() -> {
                ShipGame game = new ShipGame();
                game.setUserOneId("userOne" + userId);
                Assert.assertTrue(service.addGame(game));
                return service.getGame("userTwo" + userId) != null;
            }));
        }

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(list.toArray(new CompletableFuture[0]));
        allFutures.get();

        for (CompletableFuture<Boolean> future : list) {
            Assert.assertTrue(future.get());
        }

    }

    @Test(expected = ExecutionException.class)
    public void testGetGamePlaying() throws Exception {
        InMemoryGameStorageService service = new InMemoryGameStorageService();
        List<CompletableFuture<Boolean>> list = new ArrayList<>(100);
        for (int i = 0; i < 4; i++) {
            final int userId = i;
            list.add(CompletableFuture.supplyAsync(() -> {
                ShipGame game = new ShipGame();
                game.setUserOneId("userOne" + userId);
                Assert.assertTrue(service.addGame(game));
                return service.getGame((userId % 2 == 0) ? ("userTwo" + userId) : "existing") != null;
            }));
        }

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(list.toArray(new CompletableFuture[0]));
        allFutures.get();

    }

    @Test
    public void testCheckOpponentTimeout() throws Exception {
        InMemoryGameStorageService service = new InMemoryGameStorageService();
        ShipGame game = new ShipGame();
        game.setUserOneId("userOne");

        game.setLastUserOneSeen(System.currentTimeMillis());
        game.setLastUserTwoSeen(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1));
        service.addGame(game);
        service.getGame("userTwo");
        Assert.assertTrue(service.checkOpponentTimeout("userOne"));
        Assert.assertFalse(service.checkOpponentTimeout("userTwo"));

    }
}
