package me.echyrski.assignments.theweirdscience.service.impl;

import me.echyrski.assignments.theweirdscience.atmosphere.PubSubBroadcaster;
import me.echyrski.assignments.theweirdscience.model.GameStartInfo;
import me.echyrski.assignments.theweirdscience.model.ShipGame;
import me.echyrski.assignments.theweirdscience.service.GameStorageService;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.easymock.EasyMock;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.Future;

import static org.easymock.EasyMock.*;

/**
 * @author echyrski
 */
public class AtmosphereGameCommunicationServiceImplTest {

    @Test
    public void testCreateGameNoGame() {
        AtmosphereFramework atmosphereFramework = createMock(AtmosphereFramework.class);
        GameStorageService gameStorageService = createMock(GameStorageService.class);
        expect(gameStorageService.getGame("testUser")).andReturn(null);
        expect(gameStorageService.addGame(anyObject())).andReturn(true);
        expect(atmosphereFramework.addAtmosphereHandler(anyString(), anyObject(PubSubBroadcaster.class), anyObject(List.class))).andReturn(atmosphereFramework);
        replay(atmosphereFramework, gameStorageService);
        AtmosphereGameCommunicationServiceImpl service = new AtmosphereGameCommunicationServiceImpl(atmosphereFramework, gameStorageService);
        service.createNewGame("testUser", new Boolean[][]{});
        verify(atmosphereFramework, gameStorageService);
    }

    @Test
    public void testCreateGameExistingGame() {
        ShipGame game = new ShipGame();
        game.setGameId("gameId");
        AtmosphereFramework atmosphereFramework = createMock(AtmosphereFramework.class);
        BroadcasterFactory broadcasterFactory = createMock(BroadcasterFactory.class);
        GameStorageService gameStorageService = createMock(GameStorageService.class);
        Broadcaster broadcaster = createMock(Broadcaster.class);
        expect(gameStorageService.getGame("testUser")).andReturn(game);
        expect(atmosphereFramework.getBroadcasterFactory()).andReturn(broadcasterFactory);
        expect(broadcasterFactory.lookup("/game/gameId")).andReturn(broadcaster);
        expect(broadcaster.broadcast(anyObject(GameStartInfo.class))).andReturn(createMock(Future.class));
        replay(atmosphereFramework, gameStorageService, broadcasterFactory, broadcaster);
        AtmosphereGameCommunicationServiceImpl service = new AtmosphereGameCommunicationServiceImpl(atmosphereFramework, gameStorageService);
        service.createNewGame("testUser", new Boolean[][]{});
        verify(atmosphereFramework, gameStorageService, broadcasterFactory, broadcaster);
    }
}
