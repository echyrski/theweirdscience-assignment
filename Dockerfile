FROM     openjdk:8
MAINTAINER Eugene Chyrski
EXPOSE 8089
ADD /static /opt/assignment/static
ADD /target/theweirdscience-assignment-1.0-SNAPSHOT.jar /opt/assignment/server.jar
WORKDIR "/opt/assignment/"
ENTRYPOINT ["java","-jar","server.jar"]
